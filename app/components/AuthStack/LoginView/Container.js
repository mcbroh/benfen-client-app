import React, { Component } from 'react'
import { StatusBar, StyleSheet, View } from 'react-native';
import Modal from 'react-native-modal';
import { Button, Spinner, Text } from  'native-base';
import { TextField } from 'react-native-material-textfield';
import { connect } from 'react-redux';

import { primaryColor } from '../../../configs'
import {
  loginUser,
  emailChanged,
  passwordChanged,
  navigationHappened
} from '../../../store/auth'

class Login extends Component  {

  inputs = {};
  focusNextField = (key) => this.inputs[key].focus();

  onEmailChange = (text) => this.props.emailChanged(text);
  onPasswordChange = (text) => this.props.passwordChanged(text);
  onSubmit = () => this.props.loginUser(this.props.email, this.props.password);
  navigate = () => {
    this.props.navigationHappened()
    this.props.navigation.navigate('SignUpForm')
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle = "dark-content" hidden = {false}/>
        <Text style={styles.errorMessage}>
          {this.props.error}
        </Text>
        <View style={styles.topContainer}>
          <TextField
            containerStyle={styles.addMargin}
            label='E-mail'
            autoCorrect={false}
            keyboardType='email-address'
            autoCapitalize='none'
            value={this.props.Email}
            onChangeText={this.onEmailChange}
            error={this.props.emailError}
            returnKeyType={ "next" }
            ref={ input => this.inputs[1] = input }
            onSubmitEditing={() => this.focusNextField(2)}
          />
          <TextField
            containerStyle={styles.addMargin}
            label='Password'
            secureTextEntry
            autoCapitalize='none'
            autoCorrect={false}
            value={this.props.password}
            onChangeText={this.onPasswordChange}
            error={this.props.passwordError}
            returnKeyType={ "done" }
            ref={ input => this.inputs[2] = input }
          />

          <Button block bordered dark
            onPress={() => this.onSubmit()}
            style={styles.addMargin}
          >
            <Text> Log In </Text>
          </Button>

          <Button block
            onPress={() => this.navigate()}
            style={[styles.addMargin, styles.toBottom]}
          >
            <Text> SIGNUP </Text>
          </Button>

        </View>
          <Modal isVisible={this.props.loading} >
            <View style={styles.modalStyle}
            >
              <Spinner color={primaryColor} />
            </View>
          </Modal>
      </View>
    );
  }
}

const mapDispatchToProps = {
  loginUser,
  emailChanged,
  passwordChanged,
  navigationHappened
}

//Grab state from reducer as props
const mapStateToProps = ({ auth }) => {
  const {
    email,
    error,
    loading,
    password,
    emailError,
    passwordError,
  } = auth
  //Make value available to Component
  return {
    email,
    error,
    loading,
    password,
    emailError,
    passwordError,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eceff1',
    paddingTop: 50
  },
  topContainer: {
    flex: 1,
    zIndex: 1
  },
  BottomContainer: {
    flex: 2,
  },
  toBottom: {
    borderWidth: 1,
    borderColor: 'grey',
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 0,
  },
  addMargin: {
    marginHorizontal: 20,
    marginVertical: 10
  },
  errorMessage: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'red',
  },
  modalStyle: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)'
  }
});
