import React, { Component } from 'react'
import { StyleSheet, View, Platform } from 'react-native';
import { Header } from 'react-native-elements';
import { Button, Text } from 'native-base';
import { connect } from 'react-redux';
import { Ionicons } from '@expo/vector-icons';
import { TextField } from 'react-native-material-textfield';
import { CheckBox } from 'react-native-elements';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { primaryColor } from '../../../configs'
import {
  signupUser, 
  emailChanged,
  passwordChanged,
  rePasswordChanged, 
  navigationHappened,
  toggleTermsAgreement,
  firstNameChanged,
  lastNameChanged
} from '../../../store/auth'

class SignUp extends Component  {

  inputs = {};
  focusNextField = (key) => this.inputs[key].focus();

  onEmailChange = (text) => this.props.emailChanged(text);
  onPasswordChange = (text) => this.props.passwordChanged(text);
  onRePasswordChanged = (text) => this.props.rePasswordChanged(text);
  onLastNameChange = (text) => this.props.lastNameChanged(text);
  onFirstNameChanged = (text) => this.props.firstNameChanged(text);

  signUp = () => this.props.signupUser(
    this.props.email, this.props.password,
     this.props.lastName,
    this.props.firstName,this.props.rePassword,
  );
  _goBack = () => {
    this.props.navigationHappened()
    this.props.navigation.goBack()
  }

  render() {
    return (
      <View style={styles.container}>
      <Header
        statusBarProps={{ barStyle: 'light-content' }}
        outerContainerStyles={{ backgroundColor: 'grey' }}
        leftComponent={<Ionicons
          style={{marginTop: 15}}
          onPress={() => this._goBack()}
          name='md-arrow-back'
          size={32} color='#fff' />
        }
        centerComponent={{ text: 'SignUp',
          style: { color: '#fff', fontWeight: 'bold' }
        }}
      />
      <Text style={styles.errorMessage}>
        {this.props.error}
      </Text>
      <KeyboardAwareScrollView
      enableOnAndroid={true}
      enableAutoAutomaticScroll={(Platform.OS === 'ios')}>
        <Text style={[styles.h1, {paddingTop: 20}]}>
          Your Information
        </Text>
        <TextField
          containerStyle={styles.addMargin}
          label='First Name'
          autoCorrect={false}
          value={this.props.firstName}
          onChangeText={this.onFirstNameChanged}
          onSubmitEditing={() => this.focusNextField(2)}
          blurOnSubmit={ false }
          returnKeyType={ "next" }
          ref={ input => this.inputs[1] = input }
          error={this.props.firstNameError}
        />
        <TextField
          containerStyle={styles.addMargin}
          label='Last name'
          autoCorrect={false}
          value={this.props.lastName}
          onChangeText={this.onLastNameChange}
          error={this.props.lastNameError}
          onSubmitEditing={() => this.focusNextField(3)}
          blurOnSubmit={ false }
          returnKeyType={ "next" }
          ref={ input => this.inputs[2] = input }
        />

        <Text style={[styles.h1, {paddingTop: 20}]}>
          Your Information
        </Text>
        <TextField
          containerStyle={styles.addMargin}
          label='E-mail'
          autoCorrect={false}
          keyboardType='email-address'
          autoCapitalize='none'
          value={this.props.Email}
          onChangeText={this.onEmailChange}
          error={this.props.emailError}
          onSubmitEditing={() => this.focusNextField(4)}
          blurOnSubmit={ false }
          returnKeyType={ "next" }
          ref={ input => this.inputs[3] = input }
        />
        <TextField
          containerStyle={styles.addMargin}
          label='Password(minimum 6 characters)'
          secureTextEntry
          autoCapitalize='none'
          autoCorrect={false}
          value={this.props.Password}
          onChangeText={this.onPasswordChange}
          onSubmitEditing={() => this.focusNextField(5)}
          blurOnSubmit={ false }
          returnKeyType={ "next" }
          ref={ input => this.inputs[4] = input }
          error={this.props.passwordError}
        />
        <TextField
          containerStyle={styles.addMargin}
          label='rePassword'
          errorColor='orange'
          secureTextEntry
          autoCapitalize='none'
          autoCorrect={false}
          value={this.props.rePassword}
          onChangeText={this.onRePasswordChanged}
          error={this.props.rePasswordError}
          blurOnSubmit={ true }
          returnKeyType={ "done" }
          ref={ input => this.inputs[5] = input }
        />

        <CheckBox
          title='By signing up you accept our Terms & Policy'
          iconLeft
          iconType='material'
          checkedIcon='done'
          uncheckedIcon='crop-square'
          checkedColor={primaryColor}
          onIconPress={() => this.props.toggleTermsAgreement()}
          checked={this.props.termAgreed}
          containerStyle={{backgroundColor: 'transparent', borderWidth: 0}}
        />

        <Button block bordered dark
            onPress={() => this.signUp()}
            style={styles.addMargin}
          >
            <Text> SignUp </Text>
          </Button>

      </KeyboardAwareScrollView>
      </View>
    );
  }
}

const mapDispatchToProps = {
  signupUser, 
  emailChanged,
  passwordChanged, 
  rePasswordChanged, 
  navigationHappened,
  firstNameChanged,
  lastNameChanged,
  toggleTermsAgreement
}

//Grab state from reducer as props
const mapStateToProps = ({ auth }) => {
  const {
    email, password, error,
    lastName, firstName,
    rePassword, rePasswordError, passwordError,
    emailError, lastNameError,
    firstNameError, termAgreed
  } = auth
  //Make value available to Component
  return {
    email, password, error, lastName, firstName,
    rePassword, rePasswordError, passwordError,
    emailError, lastNameError,
    firstNameError, termAgreed
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eceff1',
  },
  topContainer: {
    flex: 1,
    marginTop: 50
  },
  BottomContainer: {
    flex: 2,
  },
  errorMessage: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'red',
  },
  h1: {
    fontSize: 28,
    fontWeight: '500',
    fontStyle: 'normal',
    color: '#bdc1cc',
    marginHorizontal: 20,
    paddingVertical: 0
  },
  addMargin: {
    marginHorizontal: 20,
    marginBottom: 10
  }
});
