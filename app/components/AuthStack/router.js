import { StackNavigator } from 'react-navigation'
import { Login, SignUp } from '../AuthStack'

export const  AuthStack = StackNavigator({
  LoginForm: {
    screen: Login,
    navigationOptions: {
      title: 'Login',
      header: null
    }
  },
  SignUpForm: {
    screen: SignUp,
    navigationOptions: {
      title: 'SignUp',
      header: null
    }
  }
})
