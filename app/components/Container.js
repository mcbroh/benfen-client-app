import React, { Component } from 'react'
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux'
import Modal from 'react-native-modal'
import { Container, Header, View, Button, Icon, Fab, Left, Body, Right, Title } from 'native-base';

import { restoreSession, logoutUser } from '../store/auth/actions'
import { AuthStack } from './AuthStack/router'
import { VerifiedStack } from './VerifiedStack/router'
import { Presentation } from './PresentationView';
import { MyQr } from './VerifiedStack';

class MainApp extends Component {
  state = {
    visibleModal: false,
    activeFAB: false
  };

  componentDidMount() {
    this.props.restoreSession()
  }

  _updateState = (_func) => {
    console.log(this.props.user.user._id);
    
    this.setState(
      { activeFAB: !this.state.activeFAB },
      () => _func
    );
  };

  render() {
    if (this.props.restoring) {
      return <Presentation />
    } else {
      if (this.props.logged) {
        return (
            <Container>
              <Header>
                <Left/>
                  <Body>
                    <Title>Header</Title>
                  </Body>
                <Right />
              </Header>
              <View style={{ flex: 1 }}>
                <VerifiedStack />
                <Fab
                  active={this.state.activeFAB}
                  direction="up"
                  containerStyle={{ }}
                  style={{ backgroundColor: 'grey' }}
                  position="bottomRight"
                  onPress={() => this._updateState(null)}
                >
                  <Icon name="share" />
                  <Button 
                    onPress={() => this.setState({ visibleModal: !this.state.visibleModal })}
                    style={{ backgroundColor: '#34A34F' }}
                  >
                    <Icon name="qrcode" type="FontAwesome"/>
                  </Button>
                  <Button 
                    onPress={() => this._updateState(this.props.logoutUser())}
                    style={{ backgroundColor: '#DD5144' }}
                  >
                    <Icon name="sign-out"  type="FontAwesome"/>
                  </Button>
                </Fab>

                <Modal
                  isVisible={this.state.visibleModal}
                  backdropColor={'black'}
                  backdropOpacity={0.80}
                  animationIn={'zoomInDown'}
                  animationOut={'zoomOutUp'}
                  animationInTiming={1000}
                  animationOutTiming={1000}
                  backdropTransitionInTiming={1000}
                  backdropTransitionOutTiming={1000}
                >
                  <MyQr 
                    onClick={() => this.setState({ visibleModal: false })}
                    userId={this.props.user.user._id}
                  />
                </Modal>
              </View>
            </Container>
        ) 
      } else {
        return <AuthStack />
      }
    }
  }
}

const mapStateToProps = ({ auth }) => ({
  logged: auth.user != null,
  restoring: auth.restoring,
  user: auth.user
})

const mapDispatchToProps = {
  restoreSession,
  logoutUser
}

export default connect(mapStateToProps, mapDispatchToProps)(MainApp);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    backgroundColor: 'lightblue',
    padding: 12,
    margin: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  }
});
