import React from 'react'
import { StyleSheet, View, Text } from 'react-native';
import { primaryColor } from '../../configs'

 const Presentation = () => {
  return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Text h1 style={styles.logoText}>
            Sylk
          </Text>
        </View>
        <View style={styles.textContainer}>
          <Text h3>Sylk</Text>
          <Text h5>When customers matters</Text>
        </View>
      </View>
  )
}

export { Presentation };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20
  },
  logoContainer: {
    width: 100,
    height: 100,
    borderRadius: 100/2,
    backgroundColor: primaryColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10
  },
  logoText: {
    color: '#fff'
  }
});
