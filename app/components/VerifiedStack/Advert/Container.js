import React, { Component } from 'react'
import { Image, StyleSheet, ScrollView } from 'react-native';
import { Body, Button, Card, CardItem, Text, Right, Left, View } from 'native-base';

class Advert extends Component  {

  _renderComponent = () => {
    if (this.props.offers.length > 0) {
      return (
        <ScrollView horizontal={true}>
          {
            this.props.offers.map((offer) => {
              return (
                <Card style={{
                  borderWidth: 0.5,
                  width: 380,
                  borderColor: 'grey'}}
                  key={offer._id}
                >
                  <CardItem header
                    style={{backgroundColor: 'red'}}
                  >
                    <Text
                      style={{color: 'white'}}
                    >{offer.adsHeader}</Text>
                  </CardItem>
                  <CardItem bordered>
                    <Body
                      style={{
                        minHeight: 40, 
                        flex: 1, 
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexWrap: 'wrap'
                      }}
                    >
                      <Text>{offer.advertMessage}</Text>
                    </Body>
                  </CardItem>
                  <CardItem bordered>
                    <Body
                      style={{
                        minHeight: 40, 
                        flex: 1, 
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexWrap: 'wrap'
                      }}
                    >
                      <Text note>{offer.adsConditions}</Text>
                    </Body>
                  </CardItem>
                  <CardItem footer>
                    <Left>
                      <Button transparent disabled
                        style={{ flexGrow: 1, alignSelf: 'flex-end' }}
                      >
                        <Text>Starts: {offer.adsStartDate}</Text>
                      </Button>
                    </Left>
                    <Right>
                      <Button transparent disabled
                        style={{ flexGrow: 1, alignSelf: 'flex-end' }}
                      >
                        <Text>Ends: {offer.adsEndDate}</Text>
                      </Button>
                    </Right>
                  </CardItem>
                </Card>
              )
            })
          }
        </ScrollView>
      )
    }

    return( 
      <Image 
        source={require('../../img/nothing.png')} 
        style={{height: 200, width: null, flex: 1}}
      />
    )
  }

  render() {
    return (
      <View style={{flex: 1}}>
        {this._renderComponent()}
      </View>
    );
  }
}

export default Advert;

const styles = StyleSheet.create({
  
});
