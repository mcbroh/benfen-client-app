import React, { Component } from 'react'
import { Image, StyleSheet, View, ScrollView } from 'react-native';
import QRCode from 'react-native-qrcode';
import { Col, Row, Grid } from "react-native-easy-grid";
import { Card, Text } from 'native-base';

const grey = '#212121';

class Check extends Component  {

  _renderComponent = () => {
    if (this.props.checks.length > 0) {
      return (
        <ScrollView horizontal={true}>
          {
            this.props.checks.map((check) => {
              return (
                <Card style={{
                  borderWidth: 0.5,
                  borderColor: grey,
                  padding: 10
                  }}
                  key={check.id}
                >
                  <Grid>
                    <Col style={{ width: 150}}>
                      <QRCode
                        value={check.id}
                        size={150}
                        bgColor= {grey}
                        fgColor='white'
                      />
                    </Col>
                    <Col>
                      <Row style={{flexDirection: "column", alignItems: "center", justifyContent: "center", borderBottomWidth: 0.5, borderColor: 'grey'}}>
                        <Text style={{fontSize: 20, fontWeight: 'bold', color: 'grey'}}>{check.value}</Text>
                      </Row>
                      <Row style={{flexDirection: "column", alignItems: "center", justifyContent: "center"}}>
                        <Text note style={{color: grey}}>{check.id}</Text>
                      </Row>
                    </Col>
                  </Grid>
                  <View  style={{flexDirection: "column", paddingTop: 10}}>
                    {
                      check.checkConditions.map((conditions, index) => {
                        return (
                          <Text key={index} note>{'\u002A  '}<Text note>{conditions.condition}</Text></Text>
                        );
                      })
                    }
                    
                  </View>
                </Card>
              );
            })
          }
        </ScrollView>
      )
    }

    return( 
      <Image 
        source={require('../../img/nothing.png')} 
        style={{height: 200, width: null, flex: 1}}
      />
    )
  }

  render() {
    return (
      <View style={{flex: 1}}>
        {this._renderComponent()}
      </View>
    );
  }
}

export default Check;

const styles = StyleSheet.create({
  
});
