import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native';
import QRCode from 'react-native-qrcode';
import { Button, Icon } from 'native-base';

const grey = '#212121';

class MyQr extends Component  {

  render() {
    return (
      <View style={styles.container}>
        <Button 
            transparent
            onPress={this.props.onClick}
        >
            <Icon active name="close-o" type="EvilIcons"  style={{fontSize: 40, color: '#fff'}}/>
        </Button>
        <QRCode
          value={this.props.userId}
          size={300}
          bgColor= {grey}
          fgColor='white'
        />
      </View>
    );
  }
}

export default MyQr;

const styles = StyleSheet.create({
  container: {
    alignItems: "center", 
    justifyContent: "center"
  }
});
