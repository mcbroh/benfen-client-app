import React, { Component } from 'react';
import { Image } from 'react-native';
import { connect } from 'react-redux'
import { Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';

import {Advert, Check} from '../../VerifiedStack';


class RewardList extends Component {
  toState = {
    check: false,
    msg: false,
  }

  state = {};

  componentWillMount(){
    this.props.data.forEach(element => {
      this.state[element.storeID] = this.toState
    });
  }

  _updateView(store, page){
    const prevState = this.state[store][page];
    this.setState({[store]: {...this.toState, [page]: !prevState}}, console.log(this.state)
    )    
  }

  changeView(store, check, offers, background) {
      if (this.state[store].check) {
        return (
          <Check 
            checks={check}
          />
        ) 
      } else if (this.state[store].msg) {
        return (
          <Advert 
            offers={offers}
          />
        );
      } else {
        return (
          <Image 
                source={{uri: background}} 
                style={{height: 200, width: null, flex: 1}}
              />
        );
      }
  };

  render() {
    
    return (
      <Content>
        {
          this.props.data.map((store) => {
            return (
              <Card key={store.storeID}>
                <CardItem>
                  <Left>
                    <Thumbnail source={{uri: store.logo}} />
                    <Body>
                      <Text>{store.storeName}</Text>
                      <Text note>{store.storeInfo}</Text>
                    </Body>
                  </Left>
                </CardItem>
                <CardItem cardBody>
                { this.changeView(store.storeID, store.checks, store.offers, store.backgroundImg) }
                </CardItem>
                <CardItem>
                  <Left>
                    <Button 
                      transparent
                      onPress={() => {this._updateView(store.storeID, 'store')}}
                    >
                      <Icon active name="activity" type="Feather"/>
                      <Text>Points: {store.points}</Text>
                    </Button>
                  </Left>
                  <Body>
                    <Button 
                      transparent
                      onPress={() => {this._updateView(store.storeID, 'msg')}}
                    >
                      <Icon active={this.state[store.storeID].msg} name="mail" />
                      <Text>Offers: {store.offers.length}</Text>
                    </Button>
                  </Body>
                  <Right>
                    <Button 
                      transparent
                      onPress={() => {this._updateView(store.storeID, 'check')}}
                    >
                      <Icon active={this.state[store.storeID].check} name="cash" />
                      <Text>Checks: {store.checks.length}</Text>
                    </Button>
                  </Right>
                </CardItem>
              </Card>  
            );
          })
        }    
      </Content>
    )
  }
}

const mapStateToProps = ({ auth }) => ({
  data: auth.user.user.points
})

export default connect(mapStateToProps)(RewardList);