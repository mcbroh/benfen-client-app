import { StackNavigator } from 'react-navigation'
import { RewardList, SignUp } from '../VerifiedStack'

export const  VerifiedStack = StackNavigator({
  LoginForm: {
    screen: RewardList,
    navigationOptions: {
      title: 'RewardList',
      header: null
    }
  },
  SignUpForm: {
    screen: SignUp,
    navigationOptions: {
      title: 'SignUp',
      header: null
    }
  }
})
