const primaryColor = '#008975' //Pesto Green
const secondaryColor = '#00AA8D'//Pesto Green Darker;

// Call to action
const callToActionColor = '#ff5722'; // red thickness variation A200

//background
const chatPrimaryColor = '#455A64';
const chatSecondaryColor = '#90A4AE';
const darkBackground = '#607D8B';

//textStyle
const lightText = '#FAFAFA';
const darkText = '#424242';
const helperText = '#BDBDBD'

 export {
   darkText,
   lightText,
   helperText,
   callToActionColor,
   primaryColor,
   secondaryColor,
   chatPrimaryColor,
   chatSecondaryColor,
   darkBackground
 };
