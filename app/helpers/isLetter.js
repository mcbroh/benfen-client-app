export const isLetter = input => {
  return input.toLowerCase() != input.toUpperCase();
}
