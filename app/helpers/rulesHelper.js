import _ from 'lodash'

export const setRules = (data, rule) => {
  const newData = arrange(data).map(obj => {
    // clone the current object
    const newObj = Object.assign({}, obj);
    // update the new object
    newObj.id === rule ? newObj.checked = true : newObj
    return newObj;
  });
  return newData;
}

const arrange = data => {
  //sort the data first in descending order
  let sortedData = _.sortBy(data, 'arrangeId')
  return sortedData.map(obj => ({...obj, checked:false}))
}

export const checkIfRuleExisit = num => {

  return Number.isInteger(num) ? num : 0

}
