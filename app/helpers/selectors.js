import _ from 'lodash'

export const getChatItems = data => {
  //sort the data first in descending order
  let sortedData = _.sortBy(data, 'createdAt')
    return sortedData ? Object.keys(sortedData).map(key => sortedData[key]) : []
}
