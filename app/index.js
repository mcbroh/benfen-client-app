import React from 'react'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducer from './store/reducer';
import MainApp from './components'

const BenFen = () => {
  const store = createStore(reducer, {}, applyMiddleware(thunk));

  return (
    <Provider store={store}>
      <MainApp />
    </Provider>
  )
}



export default BenFen