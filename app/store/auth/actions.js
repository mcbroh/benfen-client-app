import { AsyncStorage } from "react-native"
import axios from 'axios'
import * as types from './types';
import { ValidateEmail } from '../../helpers'
import { API } from '../../configs/api';

export const restoreSession = () => {
  return (dispatch) => {
    dispatch(sessionRestoring());

    AsyncStorage.getItem('token').then((result) => {
      if (result !== null) {
        // We have data!!
        dispatch(sessionSuccess(JSON.parse(result)));
      } else {
        dispatch(sessionLogout()); 
      }
    })
  }
}

export const loginUser = (email, password) => {
  if (email === '' || password === ''
      || !(ValidateEmail(email)) || password.length < 6) {
    return (dispatch) => dispatch(sessionError('Please check all fields'));
  }
  return (dispatch) => {
    dispatch(sessionLoading());

    axios.post(`${API}/customer/login`, {
      email,
      password
    })
    .then(function (res) {
      console.log(res.data);
      
      if (res.data.auth) {
        AsyncStorage.setItem('token',  JSON.stringify(res.data), () => {
          dispatch(sessionSuccess(res.data));
        });      
      } else {
        dispatch(sessionError(res.data.message));
      }
      
    })
    .catch(function (error) {
      dispatch(sessionError(error));
    });
  }
}




export const signupUser = (
  email, 
  password, 
  lastName, 
  firstName, 
  rePassword
) => {
  if (firstName === '' || lastName === '' || email === '' || password === '' ) {
    return (dispatch) => dispatch(sessionError('All fields are compulsry'))
  }
  if (password !== rePassword) {
    return (dispatch) => dispatch(
      sessionError('Password not matching')
    )
  }
  if (!(ValidateEmail(email) ) ) {
    return (dispatch) => dispatch(
      sessionError('Email or phone number badly formated')
    )
  }

  return (dispatch) => {
    dispatch(sessionLoading());

    axios.post(`${API}/customer/new`, {
      email,
      password,
      firstName,
      lastName,
      type: 'customer'
    })
    .then(function (res) {      
      if (res.data.auth) {
        AsyncStorage.setItem('token', JSON.stringify(res.data), () => {
          dispatch(sessionSuccess(res.data));
        });      
      } else {
        dispatch(sessionError(res.data.message));
      }
      
    })
    .catch(function (error) {
      dispatch(sessionError(error));
    });

  }
}

export const logoutUser = () => {
  return (dispatch) => {
    dispatch(sessionLoading());
    AsyncStorage.removeItem('token').then(() => {
        dispatch(sessionLogout());
    })
  }
}

export const emailChanged = (text) => ({
    type: types.EMAIL_CHANGED,
    payload: text
})

export const passwordChanged = (text) => ({
    type: types.PASSWORD_CHANGED,
    payload: text
})

export const rePasswordChanged = (text) => ({
    type: types.RE_PASSWORD_CHANGED,
    payload: text
})

export const firstNameChanged = (text) => ({
    type: types.FIRST_NAME_CHANGED,
    payload: text
})

export const lastNameChanged = (text) => ({
    type: types.LAST_NAME_CHANGED,
    payload: text
})


export const navigationHappened = () => ({
    type: types.NAVIGATED,
})

const sessionRestoring = () => ({
  type: types.SESSION_RESTORING
});

const sessionLoading = () => ({
  type: types.SESSION_LOADING
});

const sessionSuccess = user => ({
  type: types.SESSION_SUCCESS,
  payload: user
});

const sessionError = error => ({
  type: types.SESSION_ERROR,
  payload: error
});

export const sessionLogout = () => ({
  type: types.SESSION_LOGOUT
});

export const toggleTermsAgreement = () => ({
  type: types.TOGGLE_TERM_AGREEMENT
});

export const toggleshowTerms = () => ({
    type: types.TOGGLE_TERMS_AND_POLICY
})
