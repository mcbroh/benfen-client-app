import * as types from './types'

const INITIAL_STATE = {
  restoring: false,
  loading: false,
  user: null,
  termAgreed: false,
  email: '',
  password: '',
  rePassword: '',
  error: '',
  successMsg: '',
  lastName: '',
  firstName: '',
  //errorMessage
  rePasswordError: '',
  passwordError: '',
  emailError: '',
  lastNameError: '',
  firstNameError: '',
  //Privacy & Terms
  showTerms: false
}

const VALUES = {
  email: '',
  password: '',
  rePassword: '',
  error: '',
  successMsg: '',
  lastName: '',
  firstName: '',
  termAgreed: false,
  //errorMessage
  rePasswordError: '',
  passwordError: '',
  emailError: '',
  lastNameError: '',
  firstNameError: '',
}

const session = (state = INITIAL_STATE, action, values = VALUES) => {
  switch(action.type) {
    case types.EMAIL_CHANGED:
      return { ...state, email: action.payload, error:'', emailError:'', passwordError:''}
    case types.PASSWORD_CHANGED:
      return { ...state, password: action.payload, error: '', emailError:'', passwordError:'' }
    case types.RE_PASSWORD_CHANGED:
      if (state.password !== action.payload) {
        return { ...state, rePassword: action.payload, error: '', rePasswordError:'Not matching password' }
      }
      return { ...state, rePassword: action.payload, error: '', emailError:'', rePasswordError:'' }
    case types.SESSION_RESTORING:
      return { ...state, restoring: true }
    case types.SESSION_LOADING:
      return { ...state, restoring: false, loading: true}
    case types.SESSION_SUCCESS:
      return { ...state, restoring: false, loading: false, user: action.payload,
        ...values
            }
    case types.SESSION_ERROR:
      return {  ...state,
        loading: false,
        user: null,
        error: action.payload,
      }
    case types.TOGGLE_TERMS_AND_POLICY:
      return {...state, showTerms: !state.showTerms}
    case types.NAVIGATED:
      return {...state, ...values}
    case types.TOGGLE_TERM_AGREEMENT:
      return { ...state, termAgreed: !state.termAgreed}
    case types.SESSION_LOGOUT:
      return INITIAL_STATE
  // User Information
    case types.FIRST_NAME_CHANGED:
      return { ...state, firstName:action.payload, error:'', firstNameError:''}
    case types.LAST_NAME_CHANGED:
      return { ...state, lastName:action.payload, error:'', lastNameError:''}
    default:
      return state
  }
}

export default session
