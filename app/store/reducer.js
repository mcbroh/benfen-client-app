import { combineReducers } from 'redux'

import auth from './auth'
import user from './user'
import uiState from './uiState'

export default combineReducers({
  auth,
  user,
  uiState
})
