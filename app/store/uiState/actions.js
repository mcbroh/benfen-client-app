import * as types from './types';

export const toggleDashTabsModal = (payload) => ({
    type: types.DASH_TABS_MODAL,
    payload
})
