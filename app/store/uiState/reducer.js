import * as types from './types'

const INITIAL_STATE = {
  dashTabsModal: {
    state: false,
    component: null
  }
}

const uiState = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case types.DASH_TABS_MODAL:
      return { ...state, dashTabsModal: action.payload}
    default:
      return state
  }
}

export default uiState
