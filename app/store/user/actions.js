import { AsyncStorage } from "react-native";
import axios from 'axios'
import * as types from './types';
import { API } from '../../configs/api';
import { sessionLogout } from '../auth/actions';

export const CreateViewChanged = (text) => {
  return {
    type: types.BONUS_STAMP,
    payload: text
  };
};

export const fetchUser = () => {
  
  //Get currentUser id
  return (dispatch) => {
    AsyncStorage.getItem('token').then((result) => {
      
      axios({
        method: 'get',
        url: `${API}/stores/me`,
        params: {'Content-Type': 'application/json'},
        headers: { 'x-auth': result}
      }).then(function (res) {
        
          dispatch({
            type: types.USER_FETCH,
            payload: res.data,
          });
        
      })
      .catch(function (error) {
        // res 401. No valid token
        dispatch(sessionLogout());
      }); 
    }).done();  
  };
};


export const toggleOpen = (id, type) => {
  const openToToggle = { id, type };
  return {
    type: types.TOGGLE_OPEN,
    payload: openToToggle
  };
};


